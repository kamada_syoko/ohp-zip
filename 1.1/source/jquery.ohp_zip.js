(function($) {
    $.fn.ohp_zip = function(options){
        var defaults = {
            post :'.postalcode',
    		pref :'.pref',
            city :'.city',
            town :'.town',
            pref_k :'.pref-k',
            city_k :'.city-k',
            town_k :'.town-k'
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);
        var url  = 'http://zipcloud.ibsnet.co.jp/api/search?callback=?';
        var postalcode = '';
        var address;
        var res = new Array();

        $('body').append('<div class="ohp-zip-overlay"></div>');
        $(setting.post + ':last-child').after('<div class="ohp-zip-selector"><div class="ohp-zip-content"><div></div><button class="ohp-zip-close" tabindex="0">Close<span></span><span></span></button></div></div>');

//-------------------------------

        $(setting.post).on('focusout', function(){
            postalcode = '';

            $(setting.post).each(function(i, e) {
                postalcode = postalcode + $(e).val();
            });

            if ( postalcode.length == 7 ){
                $.getJSON(url, { zipcode: postalcode }).done(function(data) {
                    if (data.results) {
                        res = data.results;

                        if ( res.length >= 2 ){
                            $('.ohp-zip-content > div').html('');

                            $.each(res, function(i, e) {
                                $('.ohp-zip-content > div').append('<a class="ohp-zip-select" data-result="' + i + '">' + e.address1 + ' ' + e.address2 + ' ' + e.address3 + '</a>');
                            });

                            $('.ohp-zip-selector').show();
                            $('.ohp-zip-overlay').show();
                        }else{
                            setResults(res[0]);
                        }
                    }else{
                        
                    }
                });
            }
        });

//-------------------------------

        function setResults(result){
            $('input'+setting.pref).val('');
            $(setting.city).val('');
            $(setting.town).val('');
            $(setting.pref_k).val('');
            $(setting.city_k).val('');
            $(setting.town_k).val('');

            $('select'+setting.pref).val(result.prefcode);
            $('input'+setting.pref).val($('input'+setting.pref).val() + result.address1);
            $(setting.city).val($(setting.city).val() + result.address2);
            $(setting.town).focus().val($(setting.town).val() + result.address3);

            $('input'+setting.pref_k).val($('input'+setting.pref_k).val() + result.kana1);
            $(setting.city_k).val($(setting.city_k).val() + result.kana2);
            $(setting.town_k).val($(setting.town_k).val() + result.kana3);

            closeSelector();
        }

//-------------------------------

        $('.ohp-zip-content').on('click', '.ohp-zip-select', function(){
            setResults(res[$(this).attr('data-result')]);

            return false;
        });

//-------------------------------

        $('.ohp-zip-overlay, .ohp-zip-close').on('click', function(){
            closeSelector();

            return false;
        });

        $(window).keyup(function(e){
        	if ( e.keyCode == 27 ){   // ESCキー
                closeSelector();
            }
        });

//-------------------------------

        function closeSelector(){
            $('.ohp-zip-selector').hide();
            $('.ohp-zip-overlay').hide();

            return false;
        }

//-------------------------------

        return this;
    };
})(jQuery);